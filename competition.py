###################################################################
#
#   Idea: 
#   Fit a content based Model using Xgbregressor
#   Make a CF matrix with 3 graphs instead of a big matrix
#    user-> business directed graph B + U nodes
#    business -> user directed graph B + U nodes
#    business -> business directed graph 2 * B nodes
#    last graph is used to cache the similarity distance metric as test set predictions are made
#   Error Distribution:
#
#   RMSE
#
from math import isnan, nan
import multiprocessing as mp
import datetime
from json.decoder import JSONDecodeError
from os import path, sched_getaffinity, cpu_count
from sys import argv
from json import loads
from typing import cast
from pandas.core.frame import DataFrame as DF
import xgboost as xgb
import pandas as pd
import numpy as np
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer as SIA
from sklearn.model_selection import cross_validate
from sklearn.metrics import mean_squared_error, make_scorer, classification_report
from xgboost.callback import early_stop
from xgboost.sklearn import XGBClassifier


def flatten_data(y):
    out = {}

    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + '_')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(1, name + a + '_')
                i += 1
        else:
            out[name[:-1]] = x

    flatten(y)
    return out

def sentiment_score(x):
   human_bot = SIA()
   sentences = str(x).split('.') # cheap tokenize to avoid nltk
   scores = [human_bot.polarity_scores(s)['compound'] for s in sentences]
   return np.mean(scores)

def prll_sentiment_score (df: DF, column: str):
    n_cores = cpu_count()
    n_cores = min(n_cores if n_cores is not None else 0, len(sched_getaffinity(0)))
    with mp.Pool(n_cores) as pool:
        df[column] = list(pool.map(sentiment_score, df[column], chunksize=5000))


def stats(df: DF, stat_column: str, typename=float):
    df[stat_column + '_low'] = df[stat_column].apply(lambda x:  np.mean(x) if x else np.NaN)
    df[stat_column + '_hi'] = df[stat_column].apply(lambda x: np.std(x) if x else np.NaN)
    df[stat_column + '_low'] = df[stat_column + '_low'] - df[stat_column + '_hi']
    df[stat_column + '_hi'] = df[stat_column + '_low'] + df[stat_column + '_hi'].apply(lambda x:  2 * x)
    df.drop(stat_column, axis=1, inplace=True)

def age(df: DF, date_column: str, fmt="%Y-%m-%d"):
    df[date_column] = pd.to_datetime(df[date_column], format=fmt)
    df[date_column] = df[date_column].apply(lambda x: (datetime.datetime.today() - x).days)

def date_transform(df: DF, date_column: str, fmt="%Y-%m-%d"):
    df[date_column] = pd.to_datetime(df[date_column], format=fmt)
    df[date_column + '_year'] = df[date_column].dt.year
    df[date_column + '_week'] = df[date_column].dt.week
    df[date_column + '_weekday'] = df[date_column].dt.dayofweek
    df[date_column + '_day'] = df[date_column].dt.day
    df.drop(date_column, axis=1, inplace=True)

def frequency_label_transform(df: DF, label_column: str):
    counts = df[label_column].value_counts(normalize=True)
    df[label_column] = df[label_column].apply(lambda x: counts[x])

def geo_label_transform(df: DF):
    long_range = [i for i in range(-9000 * 360 - 18000, 9000 * 360 + 18001, 3)] 
    long_label = [i + 1.5 for i in range(-9000 * 360 - 18000, 9000 * 360 + 18001 - 3, 3)]
    lat_range = [i for i in range(-18000 * 180 - 9000, 18000 * 180 + 9001, 3)] 
    lat_label = [i + 1.5 for i in range(-18000 * 180 - 9000, 18000 * 180 + 9001 - 3, 3)] 
    df['long_score'] = df['latitude'].apply(lambda x: x * 100 * 360 ) + df['longitude'].apply(lambda x: x * 100)
    df['lat_score'] = df['longitude'].apply(lambda x: x * 100 * 180 ) + df['latitude'].apply(lambda x: x * 100)
    df['long_score'] = pd.cut(df['long_score'], bins=long_range, labels=long_label)
    df['lat_score'] = pd.cut(df['lat_score'], bins=lat_range, labels=lat_label)
    df['lat_score'] = df['lat_score'].astype('float')
    df['long_score'] = df['long_score'].astype('float')
    df.drop(['latitude', 'longitude'], axis=1, inplace=True)


def combine_freq_label_cols(df, cols:list, new_col:str, norm_b4_comb=True, categorical=False):
    df[new_col]= 0
    df[new_col + 'sum'] = 0
    for col_name in cols:
        counts = df[col_name].value_counts(normalize=norm_b4_comb)
        df[col_name + 'count'] = df[col_name].apply(lambda x: counts.get(x) if x is not np.NaN else np.NaN)
        df[col_name] = df[col_name + 'count'] if categorical else df[col_name]
    df[new_col + 'sum'] = df[cols].sum(axis=1, min_count=1)
    for col_name in cols:
        df[col_name] = df[col_name] * df[col_name + 'count']
        df.drop(col_name + 'count', axis=1, inplace=True)
    df[new_col] = df[cols].sum(axis=1, min_count=1)
    df.drop(cols, axis=1, inplace=True)
    df[new_col] /= df[new_col + 'sum']
    df.drop(new_col + 'sum', axis=1, inplace=True)

def tip_crunch(df : DF):
    df['weight'] = (df.groupby(by=['business_id', 'user_id'])['date'].transform('max') - df['date']).astype("timedelta64[W]")
    df['weight'] = 1 / (df['weight'] + 1)
    wmean = lambda x: np.average(x, weights=df.loc[x.index, 'weight'])
    df = df.groupby(by=['business_id', 'user_id']).aggregate({'text':wmean, 'date':max }).reset_index()
    return df

def _col_print(df: DF):
    print(df)
    with pd.option_context('display.max_rows', None,'display.max_columns', None,'display.precision', 3):
        print(df.ftypes)

class CF:
    ub_graph, bu_graph, bb_score = dict(), dict(), dict()
    def __init__(self, review_data: DF):
        for i in review_data.index:
            u, b, s= review_data['user_id'][i], review_data['business_id'][i], review_data['stars_x'][i]
            if b not in self.bu_graph.keys():
                self.bu_graph[b] = dict()
            self.bu_graph[b][u] = s
            if u not in self.ub_graph.keys():
                self.ub_graph[u] = dict()
            self.ub_graph[u][b] = s

    def sim_score(self, b1, b2):
        if b1 in self.bb_score and b2 in self.bb_score[b1]:
            return self.bb_score[b1][b2]
        elif b2 in self.bb_score and b1 in self.bb_score[b2]:
            return self.bb_score[b1][b2]
        b1u_d = self.bu_graph[b1]
        b2u_d = self.bu_graph[b2]
        common_users = b1u_d.keys() & b2u_d.keys()
        if len(common_users) < 2:  #Need atleast another user to make a co-relation
            return np.NaN
        mean1 = np.mean([b1u_d[u] for u in common_users])
        mean2 = np.mean([b2u_d[u] for u in common_users])
        if b1 not in self.bb_score.keys():
            self.bb_score[b1] = dict()
        if b2 not in self.bb_score.keys():
            self.bb_score[b2] = dict()
        self.bb_score[b1][b2] = sum([(b1u_d[u] - mean1) * (b2u_d[u] - mean2) for u in common_users])
        # 0.0000001 is to avoid divide by zero SAFELY
        self.bb_score[b1][b2] /= (sum([(b1u_d[u] - mean1) ** 2 for u in common_users]) ** 0.5 + 0.000001)  
        self.bb_score[b1][b2] /= (sum([(b2u_d[u] - mean2) ** 2 for u in common_users]) ** 0.5 + 0.000001)
        self.bb_score[b2][b1] = self.bb_score[b1][b2]
        return self.bb_score[b2][b1]
 
    def predict(self, user:str, business: str):
        count = sum([1 for b in self.ub_graph[user] if business in self.bu_graph and  b != business and self.sim_score(b, business) is not np.NaN])
        if count == 0:
            return 0, np.NaN, np.NaN
        weighted_rating_sum = sum([self.sim_score(b, business ) * self.bu_graph[b][user] for b in self.ub_graph[user] if business in self.bu_graph and  b != business and self.sim_score(b, business) is not np.NaN])
        dist_sum = sum([abs(self.sim_score(b, business)) for b in self.ub_graph[user] if business in self.bu_graph and b != business and self.sim_score(b, business) is not np.NaN]) + 0.0000001
        weighted_rating = min(5, max(0, weighted_rating_sum / dist_sum))
        # neigbor count, their mean Pearson coeffs, and the predicted rating.. former 2 used in the hybrid model train
        return count, dist_sum / count, weighted_rating

def extract_features(data_path:str, testdata_filename:str):
    users, businesses, reviews = set(), set(), dict() # Tracking sets to keep resources under the assignment scope
    user_data, business_data, train_review_data, test_review_data = DF(), DF(), DF(), DF()

    # yelptrain
    train_review_data  = cast (DF, pd.read_csv(path.join(data_path, 'yelp_train.csv')))
    for i in train_review_data.index:
        u, b= train_review_data['user_id'][i], train_review_data['business_id'][i]
        if u not in reviews.keys():
            reviews[u] = set()
        reviews[u].add(b)
        users.add(u)
        businesses.add(b)

    # yelptest
    test_review_data  =  cast(DF, pd.read_csv(testdata_filename))
    for i in test_review_data.index:
        u, b = test_review_data['user_id'][i], test_review_data['business_id'][i]
        if u not in reviews.keys():
            reviews[u] = set()
        reviews[u].add(b)
        users.add(u)
        businesses.add(b)


    print(train_review_data)
    print(test_review_data)
    #
    # # review_train.json
    # with open(path.join(data_path, "review_train.json"), 'r') as f:
    #     temp = []
    #     while True:
    #         line = f.readline()
    #         if not line:
    #             break
    #         data = loads(line)
    #         if data['user_id'] in reviews.keys() and data['business_id'] in reviews[data['user_id']]:
    #             temp.append(data)
    # review_train_data = pd.DataFrame(temp)
    # date_transform(review_train_data, 'date')
    # prll_sentiment_score(review_train_data, 'text')
    # review_train_data.drop(['review_id', 'stars'], axis=1, inplace=True)
    # review_train_data[['user_id', 'business_id']] = review_train_data[['user_id', 'business_id']].astype(str)
    # _col_print(review_train_data)
    #
    # train_review_data = train_review_data.merge(review_train_data, on=['business_id', 'user_id'], how='left')
    # test_review_data = test_review_data.merge(review_train_data, on=['business_id', 'user_id'], how='left')
    # # review_train_data = None
    # _col_print(train_review_data)
    #
    # # tip.json
    # with open(path.join(data_path, "tip.json"), 'r') as f:
    #     temp = []
    #     while True:
    #         line = f.readline()
    #         if not line:
    #             break
    #         data = loads(line)
    #         if data['user_id'] in reviews.keys() and data['business_id'] in reviews[data['user_id']]:
    #             temp.append(data)
    # tip_data = pd.DataFrame(temp)
    # prll_sentiment_score(tip_data, 'text')
    # tip_data['date'] = pd.to_datetime(tip_data['date'], format='%Y-%m-%d')
    # tip_data = tip_crunch(tip_data)
    # tip_data.drop('date', axis=1, inplace=True)
    # tip_data[['user_id', 'business_id']] = tip_data[['user_id', 'business_id']].astype(str)
    # _col_print(tip_data)
    #
    # train_review_data = train_review_data.merge(tip_data, on=['business_id', 'user_id'], how='left')
    # test_review_data = test_review_data.merge(tip_data, on=['business_id', 'user_id'], how='left')
    # # tip_data = None
    # _col_print(train_review_data)
    #

    # user.json
    with open(path.join(data_path, "user.json"), 'r') as f:
        temp = []
        while True:
            line = f.readline()
            if not line:
                break
            data = loads(line)
            if data['user_id'] in users:
                temp.append(data)
    user_data = pd.DataFrame(temp)
    user_data.drop('name', axis=1, inplace=True)
    age(user_data, 'yelping_since')
    user_data['elite'] = user_data['elite'].apply(lambda x: [int(i) for i in x.split(',')] if x != 'None' else [])
    stats(user_data, 'elite')
    user_data['friends'] = user_data['friends'].apply(lambda x: len(x.split(',')) if x != 'None' else 0 )
    #TODO Friends Avg Rating Stats
    # for sffx in ['cute', 'funny', 'hot', 'list', 'more','profile' ]:
    #     user_data.drop('compliment_' + sffx, axis=1, inplace=True)
    _col_print(user_data)

    # business.json
    with open(path.join(data_path, "business.json"), 'r') as f:
        temp = []
        while True:
            line = f.readline()
            if not line:
                break
            data = loads(line.replace('"True"','1').replace('"False"', '0').replace('True', "1").replace('False', "0"))
            if data['business_id'] in businesses:
                data = flatten_data(data)
                for k,v in data.items():
                    if k.startswith('hours_') and (k.endswith('Friday') or k.endswith('Saturday') or k.endswith('Sunday')):
                        open_t = datetime.datetime.strptime(str(v).split('-')[0], '%H:%M')
                        close_t = datetime.datetime.strptime(str(v).split('-')[1], '%H:%M')
                        open_for = (close_t - open_t).total_seconds() / 3600 if close_t > open_t else ((close_t + datetime.timedelta(1,0,0)) - open_t).total_seconds() / 3600
                        v = { 'open': open_t.hour + open_t.minute / 60, 'close': close_t.hour  + close_t.minute / 60, 'open_for': open_for }
                        data[k] = v
                    elif k.startswith('hours_'):
                        data.pop(k)
                    if k == 'categories':
                        v = [x.strip().replace(' ','_') for x in str(v).split(',')]
                        data[k] = v
                    try:
                        data[k] = loads(v.replace("'",'"')) if type(v) == str else v
                    except  JSONDecodeError:
                        data[k] = v
                data = flatten_data(data)
                temp.append(data)
    business_data = pd.DataFrame(temp)
    business_data.drop(['hours','name', 'address', 'hours_Monday', 'hours_Tuesday', 'hours_Wednesday', 'hours_Thursday','hours_Friday','hours_Saturday','hours_Sunday'], axis=1, inplace=True)
    combine_freq_label_cols(business_data, [col for col in business_data if col.startswith('categories_')], 'categories', norm_b4_comb=False, categorical=True)
    business_data.drop([col for col in business_data if col.startswith('categories_')], axis=1, inplace=True)
    combine_freq_label_cols(business_data, [col for col in business_data if col.startswith('attributes_')], 'attributes', norm_b4_comb=True, categorical=True)
    print([col for col in business_data if col.startswith('attributes_')])
    business_data.drop([col for col in business_data if col.startswith('attributes_')], axis=1, inplace=True)
    geo_label_transform(business_data) # breaking ties in longitude and latitudes to make it useful
    frequency_label_transform(business_data, 'neighborhood') 
    frequency_label_transform(business_data, 'city') 
    frequency_label_transform(business_data, 'state') 
    frequency_label_transform(business_data, 'postal_code') 
    _col_print(business_data)
  
  #   # checkin.json
   #  with open(path.join(data_path, "checkin.json"), 'r') as f:
   #      temp = []
   #      while True:
   #          line = f.readline()
   #          if not line:
   #              break
   #          data = loads(line)
   #          if data['business_id'] in businesses:
   #              temp.append(data)
   #  checkin_data = pd.DataFrame(temp)
   #  _col_print(checkin_data)
   #
   #  business_data = business_data.merge(checkin_data, on='business_id', how='left')
   #  checkin_data = None

   # photo.json
   #  with open(path.join(data_path, "photo.json"), 'r') as f:
   #      temp = []
   #      while True:
   #          line = f.readline()
   #          if not line:
   #              break
   #          data = loads(line)
   #          if data['business_id'] in businesses:
   #              temp.append(data)
   #  photo_data = pd.DataFrame(temp)
   #  print(photo_data, photo_data.dtypes)
   #
   # business_data = business_data.merge(photo_data, on='business_id', how='left')
   # photo_data = None
   # _col_print(business_data)
   
    train_review_data = train_review_data.merge(user_data, on=['user_id'], how='left')
    test_review_data = test_review_data.merge(user_data, on=['user_id'], how='left')
    user_data = None
    train_review_data = train_review_data.merge(business_data, on=['business_id'], how='left')
    test_review_data = test_review_data.merge(business_data, on=['business_id'], how='left')
    business_data = None

    _col_print(train_review_data)
    _col_print(test_review_data)
    train_review_data.to_csv('train_data', index=False)
    test_review_data.to_csv('test_data', index=False)
    return train_review_data, test_review_data 

def cv(X: DF, y: DF, X_test: DF, y_test: DF):
    X = X.append(X_test, ignore_index=True)
    y = y.append(y_test, ignore_index=True)
    params = {
        'n_jobs': -1,

    }
    scorer = make_scorer(mean_squared_error,greater_is_better=False, squared=False)
    xgbr = xgb.XGBRegressor(metrics='rmse', max_depth= 3, learning_rate=1, n_jobs=8, objective='reg:linear', missing=np.NaN)
    scores = cross_validate(estimator=xgbr,X=X,y=y,scoring=scorer, cv = 4,verbose=1)

    print(scores)
    print(np.mean(scores['test_score']))
    print(max(scores['test_score']))

def get_meta_target(mod_pred, cf_pred, truth):
    mod_err = (mod_pred - truth).apply(lambda x : abs(x))
    cf_err = (cf_pred - truth).apply(lambda x: abs(x))
    final_pred = []
    for i,_ in enumerate(truth):
        final_pred.append(1 if not np.isnan(cf_pred[i]) and mod_err[i] > cf_err[i] else 0)
        if np.isnan(cf_pred[i]) and final_pred[i] == 1:
            print(mod_pred[i], cf_pred[i], truth[i], mod_err[i],cf_err[i], final_pred[i])

    return final_pred

def get_predictions_frm_meta(mod_pred, cf_pred, meta_pred):
    final_pred = []
    for i,meta_p in enumerate(meta_pred):
        final_pred.append(cf_pred[i] if not np.isnan(cf_pred[i]) and meta_p == 1 else mod_pred[i])
        if np.isnan(cf_pred[i]) and np.isnan(final_pred[i]):
            print(mod_pred[i], cf_pred[i],meta_pred[i], final_pred[i])
    return final_pred

def evaluate_model(truth, pred):
    return (np.mean([ (p - t) ** 2 for p, t in zip(pred, truth)])) ** 0.5
    
def main(data_path: str, test_data_filename: str, out_filename: str):
    #train_data, test_data = extract_features(data_path, test_data_filename)
    n_proc = len(sched_getaffinity(0)) 
    train_data  = cast (DF, pd.read_csv('train_data'))
    test_data  = cast (DF, pd.read_csv('test_data'))
    test_ids = test_data[['user_id', 'business_id']]
    train_ids = train_data[['user_id', 'business_id']]
    
    cf = CF(train_data)
       
    # Seperate independent and Dependent Variables
    X = train_data.loc[:, (train_data.columns != 'stars_x' )& (train_data.columns != 'user_id') & (train_data.columns != 'business_id') ]
    y = train_data.loc[:, train_data.columns == 'stars_x']
    X_test = test_data.loc[:, (test_data.columns != 'stars_x' )& (test_data.columns != 'user_id') & (test_data.columns != 'business_id') ]
    y_test = test_data.loc[:, test_data.columns == 'stars_x']
    
    #cv(X, y, X_test, y_test)
    cf_train_results, cf_test_results, error = DF(), DF(), DF()
 
    # Model based training and predictions on train_set
    model = xgb.XGBRegressor(metrics='rmse', max_depth= 1, learning_rate=1, n_jobs=n_proc, objective='reg:linear', missing=np.NaN)
    model.fit(X=X, y=y) # early_stopping_rounds=50, eval_metric='rmse',eval_set=[[X_test, y_test]])
    
    pred = model.predict(X)
    rmse = evaluate_model(y['stars_x'], pd.Series(pred, dtype='float'))
    print('Model Bias',rmse)
    
    # Item CF Predictions for train_set ignoring the rating of the asked user and business
    #cf_train_results[['nbrs', 'pearson_score', 'pred']] = train_ids.apply(lambda x: cf.predict(x['user_id'],x['business_id']), axis=1, result_type='expand')
    #cf_train_results.to_csv('train_cf_res',index=False)
    cf_train_results = cast(DF, pd.read_csv('train_cf_res'))

    #  Hybrid Model - Training the classifier for choosing the better model for the data
    X_meta = X.join(cf_train_results[['nbrs', 'pearson_score']])
    y_meta = get_meta_target(pd.Series(pred), cf_train_results['pred'], y['stars_x'])
    
    model_meta = XGBClassifier(max_depth= 30, n_estimators=10000,learning_rate=0.01,min_child_weight=1, n_jobs=n_proc, objective='binary:logistic', scale_pos_weight=7,missing=np.NaN)
    
    model_meta.fit(X=X_meta, y=y_meta)
    pred_meta = model_meta.predict(X_meta)
    confusion = classification_report(y_meta, pd.Series(pred_meta, dtype='float'), zero_division=0)
    print('Hybrid Bias:'),print(confusion)

    # Model based predictions on test_set
    pred = model.predict(X_test)
    rmse = evaluate_model(y_test['stars_x'], pd.Series(pred, dtype='float'))
    print('Model Variance',rmse)
    #  [ print(i , x) for i, x in enumerate(model.feature_importances_) ]

    # Item CF Predictions for test_set
    #cf_test_results[['nbrs', 'pearson_score', 'pred']] = test_ids.apply(lambda x: cf.predict(x['user_id'],x['business_id']), axis=1, result_type='expand')
    #cf_test_results.to_csv('test_cf_res',index=False)
    cf_test_results = cast(DF, pd.read_csv('test_cf_res'))
  
    train_data.drop(['user_id', 'business_id'], axis=1, inplace=True)
    test_data.drop(['user_id', 'business_id'], axis=1, inplace=True)
    
    Xtest_meta = X_test.join(cf_test_results[['nbrs', 'pearson_score']])
    ytest_meta = get_meta_target(pd.Series(pred, dtype='float'), cf_test_results['pred'], y_test['stars_x'])
    pred_meta = model_meta.predict(Xtest_meta)
    confusion = classification_report(ytest_meta, pred_meta, zero_division=0) 
    print('Hybrid Variance'), print(confusion)
    pred = get_predictions_frm_meta(pd.Series(pred, dtype='float'), cf_test_results['pred'], pred_meta)
    rmse = mean_squared_error(y_test['stars_x'], pred, squared=False)
    print(rmse)
# error['model_pred'] = pd.Series(pred)
    #     #     # test_ids['prediction'] = test_ids['prediction'].apply(lambda x: x * 0.01) + pd.Series(pred).apply(lambda x: x * 0.99)
    # test_ids['prediction'] = (pred)
    # print(evaluate_model(y_test['stars_x'],test_ids['prediction']))
    # test_ids.to_csv(out_filename, index=False)
    

if __name__ == '__main__':
    main(argv[1], argv[2], argv[3])
