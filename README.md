# Overview
This project tries to create a hybrid recommendation system using collaborative filtering and model based approach(with xgboost). 
The dataset used is the yelp review data set.

# Steps
- Data processing for xgboost model. Bucketing of geo-coordinates and other location data points.
- Item-item recommendation using python surprise(and otherwise).
- Extract features from business to business similarity matrix with "key" businesses to model covariant data into xgboost model.
- Train and cross-validate xgboost on the resultant data. Cross validation and xgboost feature ranking is used to decide the key businesses used in previous step.
